$Id: UPGRADE.txt 3356 2013-02-15 08:31:34Z netmackan $

UPGRADE
=======

This document describes procedures needed to upgrade from one version of 
SignServer to a new version.
Upgrades within a major version, from 3.2.1 to 3.2.2 for example, are usually 
plug-in upgrades and can be made simply by deploying the new software. See 
RELEASE_NOTES.txt for more information about such upgrades.

1. Unpack the new sources
2. Make sure to copy over the configuration from the old installation or 
   use the provided signserver_build.properties.sample. Take special care 
   in reading the "notice" section of the RELEASE_NOTES.txt to see if any 
   configuration properties changed.
3. Copy over the p12 folder with keystores (if used).
4. See any special instructions for the version you are upgrading below.
5. Build and deploy as described in the installation guide.


From SignServer 3.3.x to SignServer 3.4.x
-----------------------------------------

1) Database schema change: table renamed:
   A table was renamed to GlobalConfigData. For Oracle this table was previously 
   called "GLOBALCONFIG" while for all other it was called  
   GlobalConfigurationData.
   
   - "GLOBALCONFIG" was renamed to "GlobalConfigData" (Oracle only)
   - "GlobalConfigurationData" was renamed to "GlobalConfigData" (All other)

   For Oracle:
   RENAME GLOBALCONFIG TO GlobalConfigData;

   For MySQL:
   RENAME TABLE GlobalConfigurationData TO GlobalConfigData;

   Hibernate will not rename this table but add it as a new one. For an 
   existing installation it is instead recommended to manually rename the table 
   (using the RENAME statements described above) to not loose the existing 
   configuration.

2) Database schema change: table added:
   - "AuditRecordData"

   Hibernate will try to add this table during startup of the application 
   server. However, if the database user used by the data store does not have 
   enough privileges the table has to be created manually. See 
   doc/sql-scripts/create-tables-signserver34-*.sql for the  definitions for 
   your DBMS.


From SignServer 3.2.x to SignServer 3.3.x
-----------------------------------------

1) Database schema change:

An new column in the ArchiveData table was added. Hibernate will try to add this
column during startup of the application server. However, if the database user 
used by the data store does not have enough privileges (ie. no alter table 
rights) the table has to be altered manually:

For Oracle:
ALTER TABLE ARCHIVEDATA ADD DATAENCODING NUMBER(10,0);

For MySQL:
ALTER TABLE ArchiveData ADD dataEncoding int(11) DEFAULT NULL;

After manually changing the database schema the application server should 
be restarted.

2) Dependency upgraded:

The BouncyCastle library has been upgraded to version 1.47. Users running 
Oracle JDK and JBoss will have to replace all bc*.jar copied to JBoss with 
the 1.47 version jars available under lib/ext/1.6/.
