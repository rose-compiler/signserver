
JARS

odfdom.jar is from http://odftoolkit.org/projects/odfdom. License : Apache-2.0.
Version : 0.7.5. This is patched version of 0.7.5 with patch at https://odftoolkit.org/bugzilla/show_bug.cgi?id=115

odfdom is used by ODFSigner module to parse odf documents.

NOTE: odfdom.jar is deployed as a part of signserver.ear