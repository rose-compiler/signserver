SignServer Dependencies
-----------------------
List of dependencies, versions and licenses.

$ cd lib/ext
$ find . -name "*.jar"
$ cd ../
$ find . -name "*-Lib-*.jar"


# Hibernate, license is LGPLv2.1:
./hibernate/javassist-3.4.GA.jar
./hibernate/hibernate-search-3.1.1.GA.jar
./hibernate/hibernate-annotations.jar
./hibernate/hibernate-core.jar
./hibernate/hibernate-commons-annotations.jar
./hibernate/hibernate-validator-3.1.0.GA.jar
./hibernate/hibernate-entitymanager.jar

# Hibernate dependencies, licenses are Apache/MIT/BSD like licenses:
./hibernate/antlr-2.7.6.jar
./hibernate/dom4j-1.6.1.jar
./hibernate/ejb3-persistence.jar
./hibernate/jta-1.1.jar
./hibernate/lucene-core-2.4.1.jar
./hibernate/slf4j-api-1.5.2.jar
./hibernate/slf4j-log4j12.jar

# Clover is an empty jar included for easier integration with Clover:
./clover-dir/lib/clover.jar
./no-clover/clover.jar

# Swing Appframework, license is CDDL or GPLv2+ with classpath exception:
./beans-binding/beansbinding-1.2.1.jar
./swing-app-framework/swing-worker-1.1.jar
./swing-app-framework/appframework-1.0.3.jar

# NetBeans copylibs task, license is CDDL or GPLv2+ with classpath exception:
./CopyLibs/org-netbeans-modules-java-j2seproject-copylibstask.jar

# Log4j, license is Apache
./log4j.jar

# Bouncy Castle, license is BC (BSD/X11-like)
./1.6/bcpkix-jdk15on-147.jar
./1.6/bcprov-jdk15on-147.jar

# Apache Commons, license is Apache
./ext/commons-cli-1.2.jar
./commons-collections-3.2.jar
./commons-lang-2.5.jar
./commons-fileupload-1.2.1.jar
./commons-configuration.jar
./cesecore/commons-configuration-1.5.jar
./cesecore/commons-beanutils.jar
./cesecore/commons-codec-1.3.jar
./cesecore/commons-digester-1.8.jar
./cesecore/commons-el-1.0.jar
./commons-logging.jar
./commons-io-1.4.jar
./ext/xml-apis.jar

# Cert CVC, http://ejbca.org, license is LGPLv2.1+
./cert-cvc.jar

# Velocity, license is Apache
./ext/velocity/velocity-dep-1.4.jar
./ext/velocity/xml-apis.jar
./ext/velocity/nekohtml.jar
./ext/velocity/activation.jar
./ext/velocity/jdom-b9.jar

# CESeCore, http://ejbca.org, license is LGPLv2+:
./cesecore/cesecore-client_1.1.2.jar
./cesecore/cesecore-ejb_1.1.2.jar
./cesecore/cesecore-entity_1.1.2.jar

# EHCache, license is Apache 2.0:
./cesecore/ehcache-core-2.3.2.jar

# Jackson, license is Apache 2.0:
./cesecore/jackson-all-1.7.0.jar

# OpenLDAP from 2009-10-07, license is OpenLDAP Public License
./cesecore/ldap.jar

# Quartz Scheduler, license is Apache 2.0
./quartz/quartz-1.6.0.jar

# Xerces, license is Apache 2.0
./module/ooxmlsigner/xercesImpl.jar

# Jaxen license is BSD-style
./module/ooxmlsigner/jaxen-1.1.jar

# Java EE API, GlassFish, license is CDDL or GPLv2+ with classpath exception:
./javaee-web-api-6.0/javaee-web-api-6.0.jar
./javaee-api-6.0/javaee-api-6.0.jar

# Java EE API, JBoss, license is LGPLv2.1+
./ext/ejb/jboss-ejb3x.jar
./ext/ejb/jboss-j2ee.jar

# Java API for Servlets, Apache, license is Apache
./ext/servlet-2.3.jar

# Apache XML Security J, license is Apache 2.0
./ext/xmlsec-1.4.7.jar

# Jsch, license is BSD-style
./ext/jsch-0.1.32.jar

# Xmltask, license is Apache
./ext/xmltask.jar

# JUnit, license is Common Public License
./ext/junit-4.11.jar

# Hamcrest, license is BSD new
./ext/hamcrest-core-1.3.jar

# JLine, license is BSD
./ext/jline-0.9.94.jar

# SOAP with Attachments API for Java, license is CDDL or GPLv2+
./jaxws/saaj-impl.jar
./jaxws/saaj-api.jar

# JFreeChart, license is LGPLv2.1+
./reports/jcommon-1.0.12.jar
./reports/jfreechart-1.0.9.jar

# iText, license is MPL and LGPL
./SignServer-Lib-iText.jar

# OpenXML4J, license is Apache 2.0
./SignServer-Lib-OpenXML4J.jar

# ODFDOM, license is Apache 2.0
./SignServer-Lib-ODFDOM.jar

# EJBCA Util, license is LGPLv2.1+
./SignServer-Lib-EJBCA-util.jar


