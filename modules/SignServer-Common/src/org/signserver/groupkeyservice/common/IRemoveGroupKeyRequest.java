/*************************************************************************
 *                                                                       *
 *  SignServer: The OpenSource Automated Signing Server                  *
 *                                                                       *
 *  This software is free software; you can redistribute it and/or       *
 *  modify it under the terms of the GNU Lesser General Public           *
 *  License as published by the Free Software Foundation; either         *
 *  version 2.1 of the License, or any later version.                    *
 *                                                                       *
 *  See terms of license at gnu.org.                                     *
 *                                                                       *
 *************************************************************************/
package org.signserver.groupkeyservice.common;

/**
 * Interface that all classes contain data to the
 * groupkeyservice.removeGroupKey method must implement.
 *
 * @author Philip Vendil 13 nov 2007
 * @version $Id: IRemoveGroupKeyRequest.java 1829 2011-08-10 11:50:45Z netmackan $
 */
public interface IRemoveGroupKeyRequest {
}
