/*************************************************************************
 *                                                                       *
 *  SignServer: The OpenSource Automated Signing Server                  *
 *                                                                       *
 *  This software is free software; you can redistribute it and/or       *
 *  modify it under the terms of the GNU Lesser General Public           *
 *  License as published by the Free Software Foundation; either         *
 *  version 2.1 of the License, or any later version.                    *
 *                                                                       *
 *  See terms of license at gnu.org.                                     *
 *                                                                       *
 *************************************************************************/
package org.signserver.db.cli.spi;

import org.signserver.cli.spi.CommandFactory;


/**
 * CommandFactory interface to mark command factories for the Database CLI.
 * 
 * @author Markus Kilås
 * @version $Id: DatabaseCommandFactory.java 3237 2013-01-23 13:26:11Z netmackan $
 */
public interface DatabaseCommandFactory extends CommandFactory {}
