package org.signserver.server;

public interface ITimeStampSignerLookup extends IWorkerLookup {

    public static final String TSA_REQUESTEDPOLICYOID = "TSA_REQUESTEDPOLICYOID";
}
